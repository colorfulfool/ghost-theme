.server:
	nodemon ghost/current/index.js --watch ghost/content/themes/printedpress --ext hbs,js,css
  
scss-command = scss Prototype/stylesheets/theme.scss:Prototype/printedpress/assets/theme.css -I Prototype/stylesheets/definitions

.stylesheets-server:
	 $(scss-command) --watch
 
.stylesheets:
	$(scss-command)
 
run: .server .stylesheets-server

demo: .stylesheets
	buster generate && buster deploy

publish: .stylesheets
	gscan theme && zip -r printedpress.zip theme
